/**
 * This is keep the area unit.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public enum Area implements Unit {
	SQUAREMETER("sq meter",1.00),
	SQUAREDECIMETER("sq decimeter",100),
	SQUAREMILLIMETER("sq millimeter",1000000),
	SQUAREFOOT("sq foot", 10.76),
	SQUAREINCH("sq inch", 1550),
	NGAN("ngan",0.0025),
	RAI("rai",0.000625);
	private String name;
	private double value;
	
	/**
	 * Contractor of Area
	 * @param name is name of unit.
	 * @param value is number to turn to base unit.
	 */
	private Area(String name, double value){
		this.name = name;
		this.value = value;
	}
	
	/**
	 * Get the value.
	 * @return double value
	 */
	public double getValue() {
		return this.value;
	}

	/**
	 * Get name of unit.
	 * @return String of unit.
	 */
	public String toString(){
		return this.name;
	}

}
