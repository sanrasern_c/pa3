/**
 * Enumeration that collect type of unit.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public enum UnitType {
	LENGTH{
		public Unit[] getUnits(){
			return Length.values();
		}
	},
	AREA{
		public Unit[] getUnits(){
			return Area.values();
		}
	},
	WEIGHT{
		public Unit[] getUnits(){
			return Weight.values();
		}
	},
	SPEED{
		public Unit[] getUnits(){
			return Speed.values();
		}
	};

	abstract public Unit[] getUnits();
}
