/**
 * This source code.
 */
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.zip.DataFormatException;

import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * User Interface for unit's converter.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public class ConverterUI extends JFrame {
	/**
	 * To Declare the attributes.
	 */
	private JLabel equalSign;
	private JTextField input, result;
	private JComboBox<Unit> unit1, unit2;
	private JButton convert, clear;
	private UnitConverter uc;
	private JMenuBar bar;
	private String text1;
	private String text2;

	/**
	 * Constructor of the class. 
	 * @param uc is a n UnitConverter to convert the unit.
	 */
	public ConverterUI(UnitConverter uc){
		this.uc = uc;
		initComponents();
	}

	/**
	 * Initialize the interface of the program.
	 */
	private void initComponents(){
		/** Set the title bar. */
		super.setTitle("Unit Converter");
		JPanel pane = new JPanel();
		/** Set the Layout */
		pane.setLayout(new FlowLayout());
		
		/** to set the first value */
		text1 = "";
		text2 = "";
		/** set the size of first fill box */
		input = new JTextField(10);
		/** set first fill box to empty */
		input.setText("");
		
		/**
		 * Convert when press enter when cursor on the first fill box,
		 * It will convert and show answer in second fill box.
		 * @param e is an event
		 */
		input.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					double amount = Double.parseDouble(input.getText());
					/** Can't convert when it's a negative number */
					if(amount < 0)
						throw new DataFormatException();
					Unit fromUnit = (Unit)unit1.getSelectedItem();
					Unit toUnit = (Unit)unit2.getSelectedItem();
					input.setForeground(Color.BLACK);
					result.setForeground(Color.BLACK);
					result.setText(uc.convert(amount, fromUnit, toUnit)+"");
					
					/** Turn red text when input isn't a number. */
				}catch(NumberFormatException ex){
					input.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this, "Please input number");
					/** Turn red when input negative number. */
				}catch(DataFormatException ex2){
					input.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this, "please input positive number");
				}
			}	
		});
		
		Unit[] initUnit = uc.getUnits(UnitType.LENGTH);
		/** Initialize a comboBox to collect unit */ 
		unit1 = new JComboBox<Unit>(initUnit);
		
		/** To show the equal sign */
		equalSign = new JLabel("=");
		result = new JTextField(10);
		/** Set the second fill box to blank at first program run */
		result.setText("");
		
		/**
		 * Convert when press enter when cursor on the second fill box,
		 * It will convert and show answer in first fill box.
		 * @param e is an event
		 */
		result.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					double amount = Double.parseDouble(result.getText());
					if(amount < 0)
						throw new DataFormatException();
					Unit fromUnit = (Unit)unit1.getSelectedItem();
					Unit toUnit = (Unit)unit2.getSelectedItem();
					input.setForeground(Color.BLACK);
					result.setForeground(Color.BLACK);
					input.setText(uc.convert(amount, toUnit, fromUnit)+"");
				}catch(NumberFormatException ex){
					result.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this, "Please input number");
				}catch(DataFormatException ex2){
					result.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this, "please input positive number");
				}
				
			}
		});

		/** Initialize a comboBox to collect unit */ 
		unit2 = new JComboBox<Unit>(initUnit);
		/** Initialize a button to convert */
		convert = new JButton("convert!");
		
		/**
		 * Convert the unit and answer when click convert button.
		 * @param e is an event
		 */
		convert.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				/**
				 * Answer in first fill box when
				 * 1.change number in second fill box
				 * 2.have number only in second fill box.
				 */
				try{
					if( ( !result.getText().equals(text2) && input.getText().equals(text1) && !result.getText().equals("") ) || 
							input.getText().equals("") && result.getText().equals(text2)){
						double amount = Double.parseDouble(result.getText());
						if(amount < 0){
							result.setForeground(Color.RED);
							throw new DataFormatException("first");	
						}
						Unit fromUnit = (Unit)unit1.getSelectedItem();
						Unit toUnit = (Unit)unit2.getSelectedItem();
						input.setForeground(Color.BLACK);
						result.setForeground(Color.BLACK);
						input.setText(uc.convert(amount, toUnit, fromUnit)+"");
					}
					/**
					 * if not in that 2 cases, answer in first fill box.
					 */
					else{
						JOptionPane.showMessageDialog(ConverterUI.this, "Change Right text");
						double amount = Double.parseDouble(input.getText());
						if(amount < 0)
							throw new DataFormatException("Second");
						Unit fromUnit = (Unit)unit1.getSelectedItem();
						Unit toUnit = (Unit)unit2.getSelectedItem();
						input.setForeground(Color.BLACK);
						result.setForeground(Color.BLACK);
						result.setText(uc.convert(amount, fromUnit, toUnit)+"");
					}
				/**
				 * Text red when input isn't positive number or a number. 
				 */
				}catch(NumberFormatException ex){
					result.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this, "Please input number");
				}catch(DataFormatException ex2){
					result.setForeground(Color.RED);
					JOptionPane.showMessageDialog(ConverterUI.this, "Please input positive number");
				}
				text1 = input.getText();
				text2 = result.getText();
			}
		});
		
		/**
		 * Initialize the button clear.
		 * Set the 2 fill box to be empty. 
		 */
		clear = new JButton("Clear");
		clear.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				input.setText("");
				result.setText("");
				text1 = "";
				text2 = "";
			}
		});
		
		/**
		 * Initialize a menu bar.
		 */
		bar = new JMenuBar();
		JMenu menu = new JMenu("File");

		/**
		 * Loop for create a unitType.
		 */
		for(int i=0; i<uc.getUnitTypes().length; i++){
			menu.add( new AllUnits(uc.getUnitTypes()[i] ) );
		}

		menu.add(new ExitAction());

		bar.add(menu);
		this.setJMenuBar(bar);

		/**
		 * Add stuff in to container.
		 */
		pane.add(input);
		pane.add(unit1);
		pane.add(equalSign);
		pane.add(result);
		pane.add(unit2);
		pane.add(convert);
		pane.add(clear);
		
		/** Close the program when click the cross. */
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.add(pane);
		this.pack();
		this.setVisible(true);

	}
	
	/**
	 * Method to exit the program
	 */
	public class ExitAction extends AbstractAction{
		public ExitAction(){
			super("Exit");
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}

	}

	/**
	 * Nested class for Initialize the class of each unit. 
	 */
	public class AllUnits extends AbstractAction{
		private UnitType unitType;
		/**
		 * Constructor of the class.
		 * @param unitType is a type of unit.
		 */
		public AllUnits(UnitType unitType){
			/** To show the menu */
			super(unitType.toString().toLowerCase());
			this.unitType = unitType;
		}
		
		/**
		 * Clear the text and history when select new type.
		 * Show new combo box if select another unit
		 * @param e is an event
		 */
		public void actionPerformed(ActionEvent e) {
			unit1.setModel( new DefaultComboBoxModel( unitType.getUnits()  ) );
			unit2.setModel( new DefaultComboBoxModel( unitType.getUnits()  ) );
			input.setText("");
			result.setText("");
			ConverterUI.this.pack();
		}
	}

}
