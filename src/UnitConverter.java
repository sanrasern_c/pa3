/**
 * Converter of the units.
 * @author Sanrasern Chaihetphon.
 */
public class UnitConverter {
	
	/**
	 * Convert from unit to another unit.
	 * @param amount is a value to convert.
	 * @param fromUnit is the value unit.
	 * @param toUnit is a unit that want to convert to.
	 * @return value of converted unit.
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit){
		return amount/fromUnit.getValue() * toUnit.getValue();
	}
	
	/**
	 * Get array of each unit type.
	 * @param unit is a type of unit.
	 * @return array of the unit.
	 */
	public Unit[] getUnits(UnitType unit){
		return unit.getUnits();
	}
	
	/**
	 * Get the array of the  type of units.
	 * @return array of the type of the unit.
	 */
	public UnitType[]  getUnitTypes(){
		return UnitType.values();
	}
	
}
