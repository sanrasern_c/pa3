/**
 * Give the method to the class that call this class.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public interface Unit {
	double getValue();
	String toString();
	
	
}
