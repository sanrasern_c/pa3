/**
 * This is keep the length unit.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public enum Length implements Unit {
	KILOMETER("km",0.001),
	CENTIMETER("cm",100),
	MICROMETER("micron",1000000),
	METER("m",1),
	FOOT("ft",3.281),
	INCH("inch",39.37),
	WA("wa",0.5);
	private String name;
	private double value;
	
	/**
	 * Contractor of Length
	 * @param name is name of unit.
	 * @param value is number to turn to base unit.
	 */
	private Length (String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * Get the value.
	 * @return double value
	 */
	public double getValue() {
		return this.value;
	}
	
	/**
	 * Get name of unit.
	 * @return String of unit.
	 */
	public String toString(){
		return this.name;
	}
	
	
	
	
}
