/**
 * Run the program.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public class App {
	public static void main(String [] args){
		UnitConverter uc = new UnitConverter();
		ConverterUI ui = new ConverterUI(uc);
	}

}
