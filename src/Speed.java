/**
 * This is keep the speed unit.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public enum Speed implements Unit{
	METERPERSECOND("m/s",1.0),
	KILOMETERPERHOUR("km/h",3.6),
	METERPERMINUTE("m/min",60),
	MILEPERHOUR("mph",2.237),
	FOOTPERSECOND("foot per second",3.281),
	KNOT("knot",1.944),
	SEAMILEPERHOUR("sea nile per hour",1.944);
	private String name;
	private double value;
	
	/**
	 * Contractor of Speed
	 * @param name is name of unit.
	 * @param value is number to turn to base unit.
	 */
	private Speed(String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * Get the value.
	 * @return double value
	 */
	public double getValue() {
		return this.value;
	}

	/**
	 * Get name of unit.
	 * @return String of unit.
	 */
	public String toString(){
		return this.name;
	}

}
