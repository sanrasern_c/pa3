/**
 * This is keep the weight unit.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public enum Weight implements Unit{
	KILOGRAM("kg",1.0),
	NEWTON("N",9.807),
	MILLIGRAM("mg",1000000),
	POUND("lb",2.205),
	OUNCE("oz",35.27),
	CHANG("chang",0.82);
	private String name;
	private double value;
	
	/**
	 * Contractor of Weight
	 * @param name is name of unit.
	 * @param value is number to turn to base unit.
	 */
	private Weight (String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * Get the value.
	 * @return double value
	 */
	public double getValue() {
		return this.value;
	}
	
	/**
	 * Get name of unit.
	 * @return String of unit.
	 */
	public String toString(){
		return this.name;
	}

}
